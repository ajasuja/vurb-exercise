# Vurb Engineering Exercise

## Prerequisite
	Java
	Maven

## Download

Steps:

	git clone git@bitbucket.org:ajasuja/vurb-exercise.git

## Build

Steps:

	cd ~/vurb-exercise/app/vurb-app
	
	mvn clean install
	
## Deployment

### 1). Run as Spring Boot Application

	cd ~/vurb-exercise/app/vurb-app
	
	mvn spring-boot:run

### 2).	Run as Tomcat Server

	Right-Click --> Run as Server --> Choose Server --> Deploy "vurb-app" war --> Run

## Api Endpoints

	{{vurb-url}} http://localhost:8080/vurb-app

	GET 	{{vurb-url}}/users/{{username}}/deckdetails?pageToken2={{pageTokenHash}}
	
	GET 	{{vurb-url}}/users/{{username}}/decks?pageToken1={{pageTokenHash}}
	
	GET 	{{vurb-url}}/decks/{{deck-id}}
	
	POST 	{{vurb-url}}/users/{{username}}/deck

## Sample Request/Response
	
	See wiki : https://bitbucket.org/ajasuja/vurb-exercise/wiki/Vurb%20Api%20Request-Response
	
## Code Structure
	
	Main : https://bitbucket.org/ajasuja/vurb-exercise/src/08d141e085e6503c99a05a2ddf037f856538d5b6/app/vurb-app/src/main/resources/vurb-main.png?at=master&fileviewer=file-view-default
	Test : https://bitbucket.org/ajasuja/vurb-exercise/src/08d141e085e6503c99a05a2ddf037f856538d5b6/app/vurb-app/src/main/resources/vurb-test.png?at=master&fileviewer=file-view-default
	
package com.ajasuja.vurb.dto;

import java.util.Collection;

import com.ajasuja.vurb.model.DeckMetaDatum;

public class VurbMetaResponse extends AbstractVurbResponse {

	private Collection<DeckMetaDatum> deckMetaData;

	public Collection<DeckMetaDatum> getDeckMetaData() {
		return deckMetaData;
	}

	public void setDeckMetaData(Collection<DeckMetaDatum> deckMetaData) {
		this.deckMetaData = deckMetaData;
	}

}

package com.ajasuja.vurb;

public class VurbConstants {
	public static final int MAX_RETRY_COUNT = 3;
	public static final int TIMEOUT_MILLISECONDS = 100;
	public static final int PAGE_SIZE = 10;
}

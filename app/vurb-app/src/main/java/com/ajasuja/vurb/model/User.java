package com.ajasuja.vurb.model;

import java.util.Collection;

public class User {

	private String username;
	private Collection<Deck> decks;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Collection<Deck> getDecks() {
		return decks;
	}
	public void setDecks(Collection<Deck> decks) {
		this.decks = decks;
	}
}

package com.ajasuja.vurb;

public class VurbRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public VurbRuntimeException(String message) {
		super(message);
	}
	
	public VurbRuntimeException(Throwable cause) {
		super(cause);
	}
	
	public VurbRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}

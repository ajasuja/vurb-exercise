package com.ajasuja.vurb;

public class VurbError {

	private String error;

	public VurbError(String error) {
		this.error = error;
	}
	
	public String getError() {
		return error;
	}
}

package com.ajasuja.vurb.dto;

import java.util.Collection;

import com.ajasuja.vurb.model.Deck;

public class VurbDetailResponse extends AbstractVurbResponse {

	private Collection<Deck> decks;

	public Collection<Deck> getDecks() {
		return decks;
	}

	public void setDecks(Collection<Deck> decks) {
		this.decks = decks;
	}
	
	
}

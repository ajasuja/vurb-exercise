package com.ajasuja.vurb;

public class VurbException extends Exception {

	private static final long serialVersionUID = 1L;

	public VurbException(String message) {
		super(message);
	}
	
	public VurbException(Throwable cause) {
		super(cause);
	}
	
	public VurbException(String message, Throwable cause) {
		super(message, cause);
	}

}

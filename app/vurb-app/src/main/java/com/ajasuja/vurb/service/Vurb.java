package com.ajasuja.vurb.service;

import com.ajasuja.vurb.VurbException;
import com.ajasuja.vurb.dto.VurbDetailResponse;
import com.ajasuja.vurb.dto.VurbMetaResponse;
import com.ajasuja.vurb.dto.VurbInsertResponse;
import com.ajasuja.vurb.model.Deck;

public interface Vurb {

	public VurbMetaResponse getUserMetaDecks(String username, String pageToken);

	public VurbDetailResponse getUserDetailDecks(String username, String pageToken) throws VurbException;
	
	public Deck getDeck(String id);
	
	public VurbInsertResponse addUserDeck(String username, Deck deck);
}

package com.ajasuja.vurb.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ajasuja.vurb.PageTokenAdapter;
import com.ajasuja.vurb.VurbCache;
import com.ajasuja.vurb.VurbConstants;
import com.ajasuja.vurb.dto.VurbMetaResponse;
import com.ajasuja.vurb.model.DeckMetaDatum;

/**
 * @author ajasuja
 * 
 * Pages start with 0.
 */
@Component
public class InMemoryVurbCache implements VurbCache {

	@Autowired
	private PageTokenAdapter pageTokenAdapter;
		
	public void setPageTokenAdapter(PageTokenAdapter pageTokenAdapter) {
		this.pageTokenAdapter = pageTokenAdapter;
	}
	
	private Map<String, Map<Integer, Collection<DeckMetaDatum>>> username2Page2DeckMetaData = new HashMap<String, Map<Integer,Collection<DeckMetaDatum>>>();
	private Map<String, Integer> username2ResultSize = new HashMap<String, Integer>();
	
	public Collection<DeckMetaDatum> getDeckMetaData(String username, int pageToken) {
		Map<Integer, Collection<DeckMetaDatum>> page2DeckMetaData = this.username2Page2DeckMetaData.get(username);
		if (page2DeckMetaData == null) {
			return Collections.emptyList();
		}
		return page2DeckMetaData.get(pageToken);
	}

	public int getResultSize(String username) {
		if (!this.username2ResultSize.containsKey(username)) {
			return -1;
		}
		return this.username2ResultSize.get(username);
	}

	public void addDeckMetaData(String username, DeckMetaDatum deckMetaDatum) {
		Map<Integer, Collection<DeckMetaDatum>> page2DeckMataData = null;
		int pageNumber;
		int resultSize = 0;
		if (this.username2Page2DeckMetaData.containsKey(username)) {
			page2DeckMataData = this.username2Page2DeckMetaData.get(username);
			resultSize = this.username2ResultSize.get(username);
			Integer maxPageNumber = resultSize / VurbConstants.PAGE_SIZE;
			Integer deckSizeOnLastPage = resultSize % VurbConstants.PAGE_SIZE;
			if (deckSizeOnLastPage == 0) {
				pageNumber = maxPageNumber;
				Collection<DeckMetaDatum> deckMetaData = new ArrayList<DeckMetaDatum>();
				deckMetaData.add(deckMetaDatum);
				page2DeckMataData.put(pageNumber, deckMetaData);
			} else {
				pageNumber = maxPageNumber;
				Collection<DeckMetaDatum> deckMetaData = page2DeckMataData.get(pageNumber);
				deckMetaData.add(deckMetaDatum);
			}
		} else {
			pageNumber = 0;
			page2DeckMataData = this.createNewPage(pageNumber, deckMetaDatum);
		}
		resultSize++;
		this.username2ResultSize.put(username, resultSize);
		this.username2Page2DeckMetaData.put(username, page2DeckMataData);
		this.pageTokenAdapter.encode(username, pageNumber);
	}

	private Map<Integer, Collection<DeckMetaDatum>> createNewPage(int pageNumber, DeckMetaDatum deckMetaDatum) {
		Map<Integer, Collection<DeckMetaDatum>> newPage2DeckMetaData = new HashMap<Integer, Collection<DeckMetaDatum>>();
		Collection<DeckMetaDatum> deckMetaData = new ArrayList<DeckMetaDatum>();
		deckMetaData.add(deckMetaDatum);
		newPage2DeckMetaData.put(pageNumber, deckMetaData);
		return newPage2DeckMetaData;
	}

	public int getNextPageToken(String username, int pageToken) {
		Map<Integer, Collection<DeckMetaDatum>> page2DeckMetaData = this.username2Page2DeckMetaData.get(username);
		if (page2DeckMetaData == null) {
			return -1;
		}
		int numberOfPages = page2DeckMetaData.keySet().size();
		if (pageToken < numberOfPages - 1) {
			return pageToken + 1;
		} else {
			return -1;
		}
	}

	public VurbMetaResponse getDeckMetaData(String username, String pageToken) {
		int pageTokenIntegerValue = this.pageTokenAdapter.decode(username, pageToken);
		Collection<DeckMetaDatum> deckMetaData = this.getDeckMetaData(username, pageTokenIntegerValue);
		int nextPageTokenIntegerValue = this.getNextPageToken(username, pageTokenIntegerValue);
		String pageTokenStringValue = null;
		if (nextPageTokenIntegerValue != -1) {
			pageTokenStringValue = this.pageTokenAdapter.encode(username, nextPageTokenIntegerValue);
		}
		int resultSize = this.getResultSize(username);
		return this.adaptToResponse(deckMetaData, pageTokenStringValue, resultSize);
	}
	
	private VurbMetaResponse adaptToResponse(Collection<DeckMetaDatum> deckMetaData, 
			String pageTokenStringValue, int resultSize) {
		VurbMetaResponse vurbMetaResponse = new VurbMetaResponse();
		vurbMetaResponse.setNextPageToken(pageTokenStringValue);
		vurbMetaResponse.setResultSizeEstimate(resultSize);
		vurbMetaResponse.setDeckMetaData(deckMetaData);
		return vurbMetaResponse;
	}


}

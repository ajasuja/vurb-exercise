package com.ajasuja.vurb.model;

import java.util.Collection;

public class Deck extends DeckMetaDatum {

	private Collection<Card<?>> cards;

	public Collection<Card<?>> getCards() {
		return cards;
	}

	public void setCards(Collection<Card<?>> cards) {
		this.cards = cards;
	}
	
}

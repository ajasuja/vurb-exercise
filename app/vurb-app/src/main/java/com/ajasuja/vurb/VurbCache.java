package com.ajasuja.vurb;

import com.ajasuja.vurb.dto.VurbMetaResponse;
import com.ajasuja.vurb.model.DeckMetaDatum;

public interface VurbCache {

	VurbMetaResponse getDeckMetaData(String username, String pageToken);
	
	void addDeckMetaData(String username, DeckMetaDatum deckMetaDatum);
}

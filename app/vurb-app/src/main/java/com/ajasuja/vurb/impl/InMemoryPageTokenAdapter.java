package com.ajasuja.vurb.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.ajasuja.vurb.PageTokenAdapter;

@Component
public class InMemoryPageTokenAdapter implements PageTokenAdapter {

	private Map<String, Map<String, Integer>> user2hash2pageNumber = new HashMap<String, Map<String,Integer>>();
	private Map<String, Map<Integer, String>> user2pageNumber2Hash = new HashMap<String, Map<Integer,String>>();
	
	public int decode(String username, String pageToken) {
		Map<String, Integer> hash2pageNumber = this.user2hash2pageNumber.get(username);
		if (hash2pageNumber == null) {
			return -1;
		} else {
			Integer pageNumber = hash2pageNumber.get(pageToken);
			if (pageNumber == null) {
				return -1;
			}
			return pageNumber;
		}
	}

	public String encode(String username, int pageToken) {
		Map<Integer, String> pageNumber2Hash = this.user2pageNumber2Hash.get(username);
		Map<String, Integer> hash2pageNumber = this.user2hash2pageNumber.get(username);
		String hash = null;
		if (pageNumber2Hash == null) {
			pageNumber2Hash = new HashMap<Integer, String>();
			hash2pageNumber = new HashMap<String, Integer>(); 
			hash = createHash(pageToken);
			pageNumber2Hash.put(pageToken, hash);
			hash2pageNumber.put(hash, pageToken);
			this.user2pageNumber2Hash.put(username, pageNumber2Hash);
			this.user2hash2pageNumber.put(username, hash2pageNumber);
		} else {
			if (pageNumber2Hash.containsKey(pageToken)) {
				return pageNumber2Hash.get(pageToken);
			} else {
				hash = createHash(pageToken);
				pageNumber2Hash.put(pageToken, hash);
				hash2pageNumber.put(hash, pageToken);
			}
		}
		return hash;
	}

	private String createHash(int pageToken) {
		String hash = "hash-" + String.valueOf(pageToken);
		return hash;
	}
}

package com.ajasuja.vurb.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ajasuja.vurb.VurbCache;
import com.ajasuja.vurb.VurbConstants;
import com.ajasuja.vurb.VurbDatabase;
import com.ajasuja.vurb.VurbError;
import com.ajasuja.vurb.VurbException;
import com.ajasuja.vurb.callable.VurbDatabaseCallable;
import com.ajasuja.vurb.dto.VurbDetailResponse;
import com.ajasuja.vurb.dto.VurbMetaResponse;
import com.ajasuja.vurb.dto.VurbInsertResponse;
import com.ajasuja.vurb.model.Deck;
import com.ajasuja.vurb.model.DeckMetaDatum;

@RestController
@Component
public class VurbService implements Vurb {

	@Autowired
	private VurbCache vurbCache;
	
	@Autowired
	private VurbDatabase vurbDatabase;
	
	private ExecutorService executorService = Executors.newFixedThreadPool(10);

	private Set<String> deckIds = new HashSet<String>();
	
	void setVurbCache(VurbCache vurbCache) {
		this.vurbCache = vurbCache;
	}

	void setVurbDatabase(VurbDatabase deckDao) {
		this.vurbDatabase = deckDao;
	}

	@RequestMapping(path = "/users/{username}/decks", method = RequestMethod.GET)
	public VurbMetaResponse getUserMetaDecks(@PathVariable String username, @RequestParam("pageToken1") String pageToken) {
		return this.vurbCache.getDeckMetaData(username, pageToken);
	}

	@RequestMapping(path = "/users/{username}/deckdetails", method = RequestMethod.GET)
	public VurbDetailResponse getUserDetailDecks(@PathVariable String username, @RequestParam("pageToken2") String pageToken) throws VurbException {
		VurbMetaResponse userMetaDecks = this.getUserMetaDecks(username, pageToken);
		Collection<String> deckIds = this.adapt2DeckIds(userMetaDecks.getDeckMetaData());
		int maxRetryCount = VurbConstants.MAX_RETRY_COUNT;
		Collection<Deck> allDecks = new ArrayList<Deck>();
		Collection<String> toBeFetchedDeckIds = deckIds;
		for (int currentRetryCount = 0; currentRetryCount <= maxRetryCount; currentRetryCount++) {
			Collection<Deck> fetchedDecks = this.fetchInParallel(toBeFetchedDeckIds);
			allDecks.addAll(fetchedDecks);
			if (toBeFetchedDeckIds.size() == fetchedDecks.size()) {
				toBeFetchedDeckIds = Collections.emptyList();
				break;
			}
			toBeFetchedDeckIds = this.findFailureDeckIds(toBeFetchedDeckIds, fetchedDecks);
		}
		return this.adapt2DetailResponse(userMetaDecks, allDecks, toBeFetchedDeckIds);
	}

	@RequestMapping(path = "/decks/{id}", method = RequestMethod.GET)
	public Deck getDeck(@PathVariable String id) {
		return this.vurbDatabase.fetch(id);
	}

	@RequestMapping(path = "/users/{username}/deck", method = RequestMethod.POST)
	public VurbInsertResponse addUserDeck(@PathVariable String username, @RequestBody Deck deck) {
		VurbInsertResponse vurbInsertResponse = new VurbInsertResponse();
		String deckId = deck.getId();
		if (deckIds.contains(deckId)) {
			vurbInsertResponse.setInserted(false);
			vurbInsertResponse.setId(deckId);
			return vurbInsertResponse;
		}
		deckIds.add(deckId);
		this.vurbDatabase.add(username, deck);
		DeckMetaDatum deckMetaDatum = this.toDeckMetaDatum(deck);
		this.vurbCache.addDeckMetaData(username, deckMetaDatum);
		vurbInsertResponse.setInserted(true);
		vurbInsertResponse.setId(deckId);
		return vurbInsertResponse;
	}

	private Collection<String> findFailureDeckIds(Collection<String> deckIds, Collection<Deck> decks) {
		Collection<String> failureDeckIds = new ArrayList<String>();
		Map<String, Deck> deckId2Deck = new HashMap<String, Deck>();
		for (Deck deck : decks) {
			deckId2Deck.put(deck.getId(), deck);
		}
		for (String deckId : deckIds) {
			if (!deckId2Deck.containsKey(deckId)) {
				failureDeckIds.add(deckId);
			}
		}
		return failureDeckIds;
	}

	private Collection<Deck> fetchInParallel(Collection<String> deckIds) throws VurbException {
		Collection<Future<Deck>> futures = new ArrayList<Future<Deck>>();
		Collection<Deck> decks = new ArrayList<Deck>();
		for (String deckId : deckIds) {
			VurbDatabaseCallable vurbDatabaseCallable = new VurbDatabaseCallable(deckId, vurbDatabase);
			Future<Deck> future = executorService.submit(vurbDatabaseCallable);
			futures.add(future);
		}
		for (Future<Deck> future : futures) {
			Deck deck = null;
			try {
				deck = future.get(VurbConstants.TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				throw new VurbException(e);
			} catch (ExecutionException e) {
				throw new VurbException(e);
			} catch (TimeoutException e) {
				continue;
			}
			decks.add(deck);
		}
		return decks;
	}

	private DeckMetaDatum toDeckMetaDatum(Deck deck) {
		DeckMetaDatum deckMetaDatum = new DeckMetaDatum();
		deckMetaDatum.setId(deck.getId());
		deckMetaDatum.setDesc(deck.getDesc());
		return deckMetaDatum;
	}
	
	private VurbDetailResponse adapt2DetailResponse(VurbMetaResponse vurbMetaResponse, Collection<Deck> decks, Collection<String> failedIds) {
		VurbDetailResponse vurbDetailResponse = new VurbDetailResponse();
		vurbDetailResponse.setNextPageToken(vurbMetaResponse.getNextPageToken());
		vurbDetailResponse.setResultSizeEstimate(vurbMetaResponse.getResultSizeEstimate());
		vurbDetailResponse.setDecks(decks);
		Collection<VurbError> errors = this.adapt2Errors(failedIds);
		vurbDetailResponse.setErrors(errors );
		return vurbDetailResponse;
	}

	private Collection<VurbError> adapt2Errors(Collection<String> failedIds) {
		Collection<VurbError> errors = new ArrayList<VurbError>();
		for (String failedId : failedIds) {
			errors.add(new VurbError(failedId));
		}
		return errors;
	}

	private Collection<String> adapt2DeckIds(Collection<DeckMetaDatum> deckMetaData) {
		Collection<String> deckIds = new ArrayList<String>();
 		for (DeckMetaDatum deckMetaDatum : deckMetaData) {
			deckIds.add(deckMetaDatum.getId());
		}
		return deckIds;
	}
}

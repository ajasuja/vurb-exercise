package com.ajasuja.vurb;

import java.util.Collection;

import com.ajasuja.vurb.model.Deck;

public interface VurbDatabase {

	Deck fetch(String deckId);
	
	//TODO handle timeouts
	Collection<Deck> fetch(Collection<String> deckIds);
	
	void add(String username, Deck deck);
}

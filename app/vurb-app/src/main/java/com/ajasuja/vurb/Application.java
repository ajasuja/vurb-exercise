package com.ajasuja.vurb;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com/ajasuja/vurb")
public class Application extends SpringBootServletInitializer {

	private static final String[] PROPERTIES = {};
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class).properties(PROPERTIES).run();
	}
}

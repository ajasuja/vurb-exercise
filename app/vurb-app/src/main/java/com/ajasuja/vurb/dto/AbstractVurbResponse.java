package com.ajasuja.vurb.dto;

import java.util.Collection;

import com.ajasuja.vurb.VurbError;

public class AbstractVurbResponse {

	private String nextPageToken;
	private int resultSizeEstimate;
	private Collection<VurbError> errors;
	
	public String getNextPageToken() {
		return nextPageToken;
	}
	public void setNextPageToken(String nextPageToken) {
		this.nextPageToken = nextPageToken;
	}
	public int getResultSizeEstimate() {
		return resultSizeEstimate;
	}
	public void setResultSizeEstimate(int resultSizeEstimate) {
		this.resultSizeEstimate = resultSizeEstimate;
	}
	public Collection<VurbError> getErrors() {
		return errors;
	}
	public void setErrors(Collection<VurbError> errors) {
		this.errors = errors;
	}
	
}

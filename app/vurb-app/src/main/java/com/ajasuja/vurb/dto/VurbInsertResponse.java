package com.ajasuja.vurb.dto;

public class VurbInsertResponse {

	private String id;
	private boolean isInserted;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isInserted() {
		return isInserted;
	}

	public void setInserted(boolean isUpserted) {
		this.isInserted = isUpserted;
	}
}

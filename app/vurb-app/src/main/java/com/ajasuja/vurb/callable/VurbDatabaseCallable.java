package com.ajasuja.vurb.callable;

import java.util.concurrent.Callable;

import com.ajasuja.vurb.VurbDatabase;
import com.ajasuja.vurb.model.Deck;

public class VurbDatabaseCallable implements Callable<Deck> {

	private String deckId;
	private VurbDatabase vurbDatabase;
	
	public VurbDatabaseCallable(String deckId, VurbDatabase vurbDatabase) {
		this.deckId = deckId;
		this.vurbDatabase = vurbDatabase;
	}
	
	public Deck call() throws Exception {
		return this.vurbDatabase.fetch(deckId);
	}
}

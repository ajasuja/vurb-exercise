package com.ajasuja.vurb;

public interface PageTokenAdapter {

	String encode(String username, int pageToken);
	
	int decode(String username, String pageToken);
}

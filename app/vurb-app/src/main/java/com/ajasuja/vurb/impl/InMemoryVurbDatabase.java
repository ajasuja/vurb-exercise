package com.ajasuja.vurb.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.ajasuja.vurb.VurbDatabase;
import com.ajasuja.vurb.model.Deck;

@Component
public class InMemoryVurbDatabase implements VurbDatabase {

	private Map<String, Deck> deckId2Deck = new ConcurrentHashMap<String, Deck>();
	private Map<String, Collection<String>> username2DeckIds = new ConcurrentHashMap<String, Collection<String>>();
	
	public Deck fetch(String deckId) {
		return this.deckId2Deck.get(deckId);
	}

	public Collection<Deck> fetch(Collection<String> deckIds) {
		Collection<Deck> decks = new ArrayList<Deck>();
		for (String deckId : deckIds) {
			decks.add(this.fetch(deckId));
		}
		return decks;
	}

	public void add(String username, Deck deck) {
		String deckId = deck.getId();
		this.deckId2Deck.put(deckId, deck);
		if (this.username2DeckIds.containsKey(username)) {
			Collection<String> deckIds = this.username2DeckIds.get(username);
			deckIds.add(deckId);
		} else {
			Collection<String> newDeckIds = new ArrayList<String>();
			newDeckIds.add(deckId);
			this.username2DeckIds.put(username, newDeckIds);
		}
	}

}

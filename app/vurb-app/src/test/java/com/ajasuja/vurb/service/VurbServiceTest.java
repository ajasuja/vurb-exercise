package com.ajasuja.vurb.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Test;

import com.ajasuja.vurb.AbstractVurbTest;
import com.ajasuja.vurb.VurbException;
import com.ajasuja.vurb.dto.VurbDetailResponse;
import com.ajasuja.vurb.dto.VurbMetaResponse;
import com.ajasuja.vurb.impl.InMemoryPageTokenAdapter;
import com.ajasuja.vurb.impl.InMemoryVurbCache;
import com.ajasuja.vurb.impl.InMemoryVurbDatabase;
import com.ajasuja.vurb.model.Deck;
import com.ajasuja.vurb.model.DeckMetaDatum;

public class VurbServiceTest extends AbstractVurbTest {

	@Test
	public void testAddGetOneDeck() {
		VurbService vurb = new VurbService();
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		InMemoryPageTokenAdapter pageTokenAdapter = new InMemoryPageTokenAdapter();
		vurbCache.setPageTokenAdapter(pageTokenAdapter);
		vurb.setVurbCache(vurbCache);
		vurb.setVurbDatabase(new InMemoryVurbDatabase());
		String deckId = "deck-id-1";
		Deck deck = this.createDeck(deckId, "deck-desc-1");
		String username = "testuser";
		vurb.addUserDeck(username , deck );
		
		Deck actualDeck = vurb.getDeck(deckId);
		assertNotNull(actualDeck);
	}
	
	@Test
	public void testAddGetOneDeckMeta() {
		VurbService vurb = new VurbService();
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		vurbCache.setPageTokenAdapter(new InMemoryPageTokenAdapter());
		vurb.setVurbCache(vurbCache);
		vurb.setVurbDatabase(new InMemoryVurbDatabase());
		String deckId = "deck-id-1";
		String deckDesc = "deck-desc-1";
		Deck deck = this.createDeck(deckId, deckDesc);
		String username = "testuser";
		vurb.addUserDeck(username , deck);
		
		VurbMetaResponse vurbMetaResponse = vurb.getUserMetaDecks(username, "hash-0");
		String nextPageToken = vurbMetaResponse.getNextPageToken();
		assertNull(nextPageToken);
		int resultSizeEstimate = vurbMetaResponse.getResultSizeEstimate();
		assertEquals(1, resultSizeEstimate);
		Collection<DeckMetaDatum> deckMetaData = vurbMetaResponse.getDeckMetaData();
		assertEquals(1, deckMetaData.size());
		DeckMetaDatum deckMetaDatum1 = deckMetaData.iterator().next();
		assertEquals(deckId, deckMetaDatum1.getId());
		assertEquals(deckDesc, deckMetaDatum1.getDesc());
	}

	@Test
	public void testAddGet3DecksMetaSameUser() {
		VurbService vurb = new VurbService();
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		vurbCache.setPageTokenAdapter(new InMemoryPageTokenAdapter());
		vurb.setVurbCache(vurbCache);
		vurb.setVurbDatabase(new InMemoryVurbDatabase());
		String deckId1 = "deck-id-1";
		String deckDesc1 = "deck-desc-1";
		String deckId2 = "deck-id-2";
		String deckDesc2 = "deck-desc-2";
		String deckId3 = "deck-id-3";
		String deckDesc3 = "deck-desc-3";
		Deck deck1 = this.createDeck(deckId1, deckDesc1);
		Deck deck2 = this.createDeck(deckId2, deckDesc2);
		Deck deck3 = this.createDeck(deckId3, deckDesc3);
		String username = "testuser";
		vurb.addUserDeck(username , deck1);
		vurb.addUserDeck(username , deck2);
		vurb.addUserDeck(username , deck3);
		
		VurbMetaResponse vurbMetaResponse = vurb.getUserMetaDecks(username, "hash-0");
		String nextPageToken = vurbMetaResponse.getNextPageToken();
		assertNull(nextPageToken);
		int resultSizeEstimate = vurbMetaResponse.getResultSizeEstimate();
		assertEquals(3, resultSizeEstimate);
		Collection<DeckMetaDatum> deckMetaData = vurbMetaResponse.getDeckMetaData();
		assertEquals(3, deckMetaData.size());
		Iterator<DeckMetaDatum> deckMetaDataIterator = deckMetaData.iterator();
		DeckMetaDatum deckMetaDatum1 = deckMetaDataIterator.next();
		assertEquals(deckId1, deckMetaDatum1.getId());
		assertEquals(deckDesc1, deckMetaDatum1.getDesc());
		DeckMetaDatum deckMetaDatum2 = deckMetaDataIterator.next();
		assertEquals(deckId2, deckMetaDatum2.getId());
		assertEquals(deckDesc2, deckMetaDatum2.getDesc());
		DeckMetaDatum deckMetaDatum3 = deckMetaDataIterator.next();
		assertEquals(deckId3, deckMetaDatum3.getId());
		assertEquals(deckDesc3, deckMetaDatum3.getDesc());
	}
	
	@Test
	public void testAddGet3DeckDetailsSameUser() throws VurbException {
		VurbService vurb = new VurbService();
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		vurbCache.setPageTokenAdapter(new InMemoryPageTokenAdapter());
		vurb.setVurbCache(vurbCache);
		vurb.setVurbDatabase(new InMemoryVurbDatabase());
		String deckId1 = "deck-id-1";
		String deckDesc1 = "deck-desc-1";
		String deckId2 = "deck-id-2";
		String deckDesc2 = "deck-desc-2";
		String deckId3 = "deck-id-3";
		String deckDesc3 = "deck-desc-3";
		Deck deck1 = this.createDeck(deckId1, deckDesc1);
		Deck deck2 = this.createDeck(deckId2, deckDesc2);
		Deck deck3 = this.createDeck(deckId3, deckDesc3);
		String username = "testuser";
		vurb.addUserDeck(username , deck1);
		vurb.addUserDeck(username , deck2);
		vurb.addUserDeck(username , deck3);
		
		VurbDetailResponse vurbDetailResponse = vurb.getUserDetailDecks(username, "hash-0");
		String nextPageToken = vurbDetailResponse.getNextPageToken();
		assertNull(nextPageToken);
		int resultSizeEstimate = vurbDetailResponse.getResultSizeEstimate();
		assertEquals(3, resultSizeEstimate);
		Collection<Deck> decks = vurbDetailResponse.getDecks();
		assertTrue(decks.contains(deck1));
		assertTrue(decks.contains(deck2));
		assertTrue(decks.contains(deck3));
	}
	
	@Test
	public void testAddGet12DecksMetaSameUser() {
		VurbService vurb = new VurbService();
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		vurbCache.setPageTokenAdapter(new InMemoryPageTokenAdapter());
		vurb.setVurbCache(vurbCache);
		vurb.setVurbDatabase(new InMemoryVurbDatabase());
		Deck deck1 = this.createDeck("deck-id-1", "deck-desc-1");
		Deck deck2 = this.createDeck("deck-id-2", "deck-desc-2");
		Deck deck3 = this.createDeck("deck-id-3", "deck-desc-3");
		Deck deck4 = this.createDeck("deck-id-4", "deck-desc-4");
		Deck deck5 = this.createDeck("deck-id-5", "deck-desc-5");
		Deck deck6 = this.createDeck("deck-id-6", "deck-desc-6");
		Deck deck7 = this.createDeck("deck-id-7", "deck-desc-7");
		Deck deck8 = this.createDeck("deck-id-8", "deck-desc-8");
		Deck deck9 = this.createDeck("deck-id-9", "deck-desc-9");
		Deck deck10 = this.createDeck("deck-id-10", "deck-desc-10");
		Deck deck11 = this.createDeck("deck-id-11", "deck-desc-11");
		Deck deck12 = this.createDeck("deck-id-12", "deck-desc-12");
		String username = "testuser";
		vurb.addUserDeck(username , deck1);
		vurb.addUserDeck(username , deck2);
		vurb.addUserDeck(username , deck3);
		vurb.addUserDeck(username , deck4);
		vurb.addUserDeck(username , deck5);
		vurb.addUserDeck(username , deck6);
		vurb.addUserDeck(username , deck7);
		vurb.addUserDeck(username , deck8);
		vurb.addUserDeck(username , deck9);
		vurb.addUserDeck(username , deck10);
		vurb.addUserDeck(username , deck11);
		vurb.addUserDeck(username , deck12);
		
		VurbMetaResponse vurbMetaResponsePage0 = vurb.getUserMetaDecks(username, "hash-0");
		String nextPageToken = vurbMetaResponsePage0.getNextPageToken();
		assertNotNull(nextPageToken);
		int resultSizeEstimate = vurbMetaResponsePage0.getResultSizeEstimate();
		assertEquals(12, resultSizeEstimate);
		Collection<DeckMetaDatum> deckMetaData = vurbMetaResponsePage0.getDeckMetaData();
		assertEquals(10, deckMetaData.size());
		
		VurbMetaResponse vurbMetaResponsePage1 = vurb.getUserMetaDecks(username, "hash-1");
		assertNull(vurbMetaResponsePage1.getNextPageToken());
		assertEquals(12, vurbMetaResponsePage1.getResultSizeEstimate());
		assertEquals(2, vurbMetaResponsePage1.getDeckMetaData().size());
	}

	
	@Test
	public void testAddGet3DecksDifferentUser() {
		VurbService vurb = new VurbService();
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		vurbCache.setPageTokenAdapter(new InMemoryPageTokenAdapter());
		vurb.setVurbCache(vurbCache);
		vurb.setVurbDatabase(new InMemoryVurbDatabase());
		String deckId1 = "deck-id-1";
		String deckDesc1 = "deck-desc-1";
		String deckId2 = "deck-id-2";
		String deckDesc2 = "deck-desc-2";
		String deckId3 = "deck-id-3";
		String deckDesc3 = "deck-desc-3";
		Deck deck1 = this.createDeck(deckId1, deckDesc1);
		Deck deck2 = this.createDeck(deckId2, deckDesc2);
		Deck deck3 = this.createDeck(deckId3, deckDesc3);
		String username1 = "testuser1";
		String username2 = "testuser2";
		vurb.addUserDeck(username1 , deck1);
		vurb.addUserDeck(username1 , deck2);
		vurb.addUserDeck(username2 , deck3);
		
		VurbMetaResponse vurbMetaResponseUser1 = vurb.getUserMetaDecks(username1, "hash-0");
		assertNull(vurbMetaResponseUser1.getNextPageToken());
		assertEquals(2, vurbMetaResponseUser1.getResultSizeEstimate());
		assertEquals(2, vurbMetaResponseUser1.getDeckMetaData().size());
		DeckMetaDatum user1deck1 = vurbMetaResponseUser1.getDeckMetaData().iterator().next();
		assertEquals(deckId1, user1deck1.getId());
		assertEquals(deckDesc1, user1deck1.getDesc());
		
		VurbMetaResponse vurbMetaResponseUser2 = vurb.getUserMetaDecks(username2, "hash-0");
		assertNull(vurbMetaResponseUser2.getNextPageToken());
		assertEquals(1, vurbMetaResponseUser2.getResultSizeEstimate());
		assertEquals(1, vurbMetaResponseUser2.getDeckMetaData().size());
		DeckMetaDatum user2deck1 = vurbMetaResponseUser2.getDeckMetaData().iterator().next();
		assertEquals(deckId3, user2deck1.getId());
		assertEquals(deckDesc3, user2deck1.getDesc());
	}
	
	@Test
	public void testAddGet3DecksDetailsDifferentUser() throws VurbException {
		VurbService vurb = new VurbService();
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		vurbCache.setPageTokenAdapter(new InMemoryPageTokenAdapter());
		vurb.setVurbCache(vurbCache);
		vurb.setVurbDatabase(new InMemoryVurbDatabase());
		String deckId1 = "deck-id-1";
		String deckDesc1 = "deck-desc-1";
		String deckId2 = "deck-id-2";
		String deckDesc2 = "deck-desc-2";
		String deckId3 = "deck-id-3";
		String deckDesc3 = "deck-desc-3";
		Deck deck1 = this.createDeck(deckId1, deckDesc1);
		Deck deck2 = this.createDeck(deckId2, deckDesc2);
		Deck deck3 = this.createDeck(deckId3, deckDesc3);
		String username1 = "testuser1";
		String username2 = "testuser2";
		vurb.addUserDeck(username1 , deck1);
		vurb.addUserDeck(username1 , deck2);
		vurb.addUserDeck(username2 , deck3);
		
		VurbDetailResponse vurbDetailResponseUser1 = vurb.getUserDetailDecks(username1, "hash-0");
		assertNull(vurbDetailResponseUser1.getNextPageToken());
		assertEquals(2, vurbDetailResponseUser1.getResultSizeEstimate());
		assertEquals(2, vurbDetailResponseUser1.getDecks().size());
		assertTrue(vurbDetailResponseUser1.getDecks().contains(deck1));
		assertTrue(vurbDetailResponseUser1.getDecks().contains(deck2));
		assertFalse(vurbDetailResponseUser1.getDecks().contains(deck3));

		VurbDetailResponse vurbDetailResponseUser2 = vurb.getUserDetailDecks(username2, "hash-0");
		assertNull(vurbDetailResponseUser2.getNextPageToken());
		assertEquals(1, vurbDetailResponseUser2.getResultSizeEstimate());
		assertEquals(1, vurbDetailResponseUser2.getDecks().size());
		assertFalse(vurbDetailResponseUser2.getDecks().contains(deck1));
		assertFalse(vurbDetailResponseUser2.getDecks().contains(deck2));
		assertTrue(vurbDetailResponseUser2.getDecks().contains(deck3));
	}

}

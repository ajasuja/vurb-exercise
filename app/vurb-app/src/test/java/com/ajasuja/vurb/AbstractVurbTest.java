package com.ajasuja.vurb;

import java.util.ArrayList;
import java.util.Collection;

import com.ajasuja.vurb.model.Card;
import com.ajasuja.vurb.model.Deck;
import com.ajasuja.vurb.model.DeckMetaDatum;
import com.ajasuja.vurb.model.Movie;
import com.ajasuja.vurb.model.Place;

public class AbstractVurbTest {

	public Deck createDeck(String deckId, String deckDesc) {
		Deck deck = new Deck();
		deck.setId(deckId);
		deck.setDesc(deckDesc);
		Collection<Card<?>> cards = new ArrayList<Card<?>>();
		cards.add(this.createPlaceCard("card-id-1", "card-title-1", "place-1", "place-desc-1"));
		cards.add(this.createMovieCard("card-id-2", "card-title-2", "movie-1", "movie-desc-1"));
		deck.setCards(cards);
		return deck;
	}

	public DeckMetaDatum createDeckMeta(String deckId, String deckDesc) {
		DeckMetaDatum deckMetaDatum = new DeckMetaDatum();
		deckMetaDatum.setId(deckId);
		deckMetaDatum.setDesc(deckDesc);
		return deckMetaDatum;
	}
	
	public Card<Place> createPlaceCard(String cardId, String cardTitle, String placeId, String placeDescription) {
		Card<Place> placeCard = new Card<Place>();
		placeCard.setId(cardId);
		placeCard.setTitle(cardTitle);
		Place place = new Place();
		place.setPlaceId(placeId);
		place.setPlaceDescription(placeDescription);
		placeCard.setPayload(place);
		return placeCard;
	}
	
	public Card<Movie> createMovieCard(String cardId, String cardTitle, String movieId, String movieDescription) {
		Card<Movie> placeCard = new Card<Movie>();
		placeCard.setId(cardId);
		placeCard.setTitle(cardTitle);
		Movie place = new Movie();
		place.setMovieId(movieId);
		place.setMovieDescription(movieDescription);
		placeCard.setPayload(place);
		return placeCard;
	}

}

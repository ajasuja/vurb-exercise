package com.ajasuja.vurb.serialize;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonSerializer<T> {

	public String serialize(T object) throws SerializerException {
		return this.serialize(object, false);
	}

	public String serialize(T object, boolean prettyPrint) throws SerializerException {
		ObjectMapper mapper = new ObjectMapper();
		if (prettyPrint) {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
		}
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			String message = String.format("can't serialize the bean");
			throw new SerializerException(message, e);
		}
	}

}

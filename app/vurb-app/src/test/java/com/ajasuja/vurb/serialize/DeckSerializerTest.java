package com.ajasuja.vurb.serialize;

import org.junit.Test;

import com.ajasuja.vurb.AbstractVurbTest;
import com.ajasuja.vurb.model.Deck;

public class DeckSerializerTest extends AbstractVurbTest {

	@Test
	public void testDeckSerializer1() throws SerializerException {
		Deck deck = this.createDeck("deck-id-1", "deck-desc-1");
		JsonSerializer<Deck> jsonSerializer = new JsonSerializer<Deck>();
		jsonSerializer.serialize(deck, true);
	}
}

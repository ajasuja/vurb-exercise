package com.ajasuja.vurb.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.ajasuja.vurb.AbstractVurbTest;
import com.ajasuja.vurb.PageTokenAdapter;
import com.ajasuja.vurb.dto.VurbMetaResponse;
import com.ajasuja.vurb.model.DeckMetaDatum;

public class InMemoryVurbCacheTest extends AbstractVurbTest {

	@Test
	public void test1() {
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		PageTokenAdapter pageTokenAdapter = new InMemoryPageTokenAdapter();
		vurbCache.setPageTokenAdapter(pageTokenAdapter);
		String username = "testuser";
		DeckMetaDatum deckMetaDatum = this.createDeckMeta("deck-id-1", "deck-desc-1");
		vurbCache.addDeckMetaData(username , deckMetaDatum);
		String pageToken = "hash-0";
		VurbMetaResponse vurbMetaResponse = vurbCache.getDeckMetaData(username, pageToken);
		assertNull(vurbMetaResponse.getNextPageToken());
		assertEquals(1, vurbMetaResponse.getResultSizeEstimate());
		assertEquals(1, vurbMetaResponse.getDeckMetaData().size());
	}
	
	@Test
	public void test12() {
		InMemoryVurbCache vurbCache = new InMemoryVurbCache();
		PageTokenAdapter pageTokenAdapter = new InMemoryPageTokenAdapter();
		vurbCache.setPageTokenAdapter(pageTokenAdapter);
		String username = "testuser";
		DeckMetaDatum deckMetaDatum1 = this.createDeckMeta("deck-id-1", "deck-desc-1");
		DeckMetaDatum deckMetaDatum2 = this.createDeckMeta("deck-id-2", "deck-desc-2");
		DeckMetaDatum deckMetaDatum3 = this.createDeckMeta("deck-id-3", "deck-desc-3");
		DeckMetaDatum deckMetaDatum4 = this.createDeckMeta("deck-id-4", "deck-desc-4");
		DeckMetaDatum deckMetaDatum5 = this.createDeckMeta("deck-id-5", "deck-desc-5");
		DeckMetaDatum deckMetaDatum6 = this.createDeckMeta("deck-id-6", "deck-desc-6");
		DeckMetaDatum deckMetaDatum7 = this.createDeckMeta("deck-id-7", "deck-desc-7");
		DeckMetaDatum deckMetaDatum8 = this.createDeckMeta("deck-id-8", "deck-desc-8");
		DeckMetaDatum deckMetaDatum9 = this.createDeckMeta("deck-id-9", "deck-desc-9");
		DeckMetaDatum deckMetaDatum10 = this.createDeckMeta("deck-id-10", "deck-desc-10");
		DeckMetaDatum deckMetaDatum11 = this.createDeckMeta("deck-id-11", "deck-desc-1");
		DeckMetaDatum deckMetaDatum12 = this.createDeckMeta("deck-id-12", "deck-desc-12");
		vurbCache.addDeckMetaData(username , deckMetaDatum1);
		vurbCache.addDeckMetaData(username , deckMetaDatum2);
		vurbCache.addDeckMetaData(username , deckMetaDatum3);
		vurbCache.addDeckMetaData(username , deckMetaDatum4);
		vurbCache.addDeckMetaData(username , deckMetaDatum5);
		vurbCache.addDeckMetaData(username , deckMetaDatum6);
		vurbCache.addDeckMetaData(username , deckMetaDatum7);
		vurbCache.addDeckMetaData(username , deckMetaDatum8);
		vurbCache.addDeckMetaData(username , deckMetaDatum9);
		vurbCache.addDeckMetaData(username , deckMetaDatum10);

		vurbCache.addDeckMetaData(username , deckMetaDatum11);
		vurbCache.addDeckMetaData(username , deckMetaDatum12);
		
		String pageToken0 = "hash-0";
		VurbMetaResponse vurbMetaResponse0 = vurbCache.getDeckMetaData(username, pageToken0);
		assertNotNull(vurbMetaResponse0.getNextPageToken());
		assertEquals(12, vurbMetaResponse0.getResultSizeEstimate());
		assertEquals(10, vurbMetaResponse0.getDeckMetaData().size());
		
		String pageToken1 = "hash-1";
		VurbMetaResponse vurbMetaResponse1 = vurbCache.getDeckMetaData(username, pageToken1);
		assertNull(vurbMetaResponse1.getNextPageToken());
		assertEquals(12, vurbMetaResponse1.getResultSizeEstimate());
		assertEquals(2, vurbMetaResponse1.getDeckMetaData().size());
		
	}

}
